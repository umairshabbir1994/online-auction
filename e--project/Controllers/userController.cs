﻿using Microsoft.AspNetCore.Mvc;

namespace e__project.Controllers
{
    public class userController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

		public IActionResult IndexCar()
		{
			return View();
		}
		public IActionResult Dashboard()
		{
			return View();
		}
		public IActionResult IndexLand()
		{
			return View();
		}

		public IActionResult Auction()
		{
			return View();
		}

		public IActionResult AuctionDetails()
		{
			return View();
		}

		public IActionResult About()
		{
			return View();
		}

		public IActionResult Winner()
		{
			return View();
		}

		public IActionResult Login()
		{
			return View();
		}

		public IActionResult Register()
		{
			return View();
		}

		public IActionResult Faq()
		{
			return View();
		}

		public IActionResult PrivacyPolicy()
		{
			return View();
		}

		public IActionResult Error()
		{
			return View();
		}

        public IActionResult Blog()
        {
            return View();
        }

        public IActionResult BlogStandard()
        {
            return View();
        }

        public IActionResult BlogDetails()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }
    }
}
